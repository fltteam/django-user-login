This is a demo app for user login api created with Django and Django Rest Framework

The Endpoints include:

Post request to {root_url}/api/token/auth/          -For loging-in a user


Post request to {root_url}/api/users/register/      -For regestering a user


Get request to  {root_url}/api/users/logout/        -For loging-out a user


Get request to  {root_url}/api/users/               -To View All system users
